import * as React from 'react';
import { Text,Button, TextInput,View,TouchableOpacity, StyleSheet, Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack'

import ProjectScreen from './components/ProjectScreen';
import ProjectPartScreen from './components/ProjectPartScreen';
import LoginScreen from './components/LoginScreen';
import ProjectDetailsScreen from './components/ProjectDetailsScreen';
import FinanceScreen from './components/FinanceScreen';
import FinanceDetailsScreen from './components/FinanceDetailsScreen';
import ProjectPartDetailsScreen from './components/ProjectPartDetailsScreen';
import WorkerScreen from './components/WorkerScreen';
import ExternalPartScreen from './components/ExternalPartScreen';
import CreateProjectScreen from './components/CreateProjectScreen';
import CreateProjectPartScreen from './components/CreateProjectPartScreen';
import CreateFinanceScreen from './components/CreateFinanceScreen';
import AddWorkerScreen from './components/AddWorkerScreen';

const MainNavigatorStack = createStackNavigator(
  {
    Project: {screen: ProjectScreen},
    ProjectPart: {screen: ProjectPartScreen},
    Login: {screen: LoginScreen},
    ProjectDetails: {screen: ProjectDetailsScreen},
    Finance:{screen: FinanceScreen},
    FinanceDetails:{screen: FinanceDetailsScreen},
    ProjectPartDetails:{screen: ProjectPartDetailsScreen},
    Worker:{screen:WorkerScreen},
    ExternalPart:{screen:ExternalPartScreen},
    CreateProject:{screen:CreateProjectScreen},
    CreateProjectPart:{screen:CreateProjectPartScreen},
    CreateFinance:{screen:CreateFinanceScreen},
    AddWorker:{screen:AddWorkerScreen}
  },
  {
    initialRouteName: "Login",
    defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#87CEFA',
    },
    headerTintColor: '#0060a0',
    headerTitleStyle: {
      fontWeight: 'bold',
    }
  },
  }
);
const App = createAppContainer(MainNavigatorStack);
export default App
