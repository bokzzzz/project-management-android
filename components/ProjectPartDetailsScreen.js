import * as React from 'react';
import { Text,Button, TextInput,View,TouchableOpacity, StyleSheet, Image } from 'react-native';

export default class ProjectPartDetailsScreen extends React.Component {
    static navigationOptions = {
      title: 'Project Part Details',
    };
    render() {
      return (
        <View>
        <View style={styles.container1}>
        <Text style={styles.prviRed} >Job:{this.props.navigation.getParam("part", "").job}</  Text>
        </View>
        <View style={styles.container2}>
        <Text style={styles.drugiRed}>Man Hour:{this.props.navigation.getParam("part", "").man_hour}</Text>
        </View>
        <View style={styles.container1}>
        <Text style={styles.prviRed}>Percentage Done:{this.props.navigation.getParam("part", "").percentage_done}</Text>
        </View>
        <View style={styles.container2}>
        <Text style={styles.drugiRed}>Start Date:{this.props.navigation.getParam("part", "").start_date}</Text>
        </View>
        <View style={styles.container1}>
        <Text style={styles.prviRed}>End Date:{this.props.navigation.getParam("part", "").end_date}</Text>
        </View>
        <Button style={styles.baton} title='View Workers' onPress={() => this.props.navigation.navigate('Worker', {
          part:this.props.navigation.getParam("part", ""),
          user:this.props.navigation.getParam("user", "")})}>></Button>
        <Button style={styles.baton} title='View External Participant' onPress={() => this.props.navigation.navigate('ExternalPart', {part:this.props.navigation.getParam("part", "")})}>></Button>
        </View>
      );
    }
  }
  const styles = StyleSheet.create({
  prviRed:
  {
    fontSize:22
  },
  drugiRed:
  {
    fontSize:22
  },
  container1: {
    width:'100%',
    backgroundColor: '#e0dede',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    height:45
    
  },
   container2: {
     width:'100%',
    backgroundColor: '#f5f5f5',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    height:45
    
  },
  baton:{
    fontSize:22,
    height:45
  }
});