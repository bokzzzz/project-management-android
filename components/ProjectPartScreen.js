import * as React from 'react';
import { Text,Button, TextInput,View,TouchableOpacity, StyleSheet, Image } from 'react-native';

export default class ProjectPartScreen extends React.Component {
 static navigationOptions = {
      title: 'Project Part',
    };

    constructor() {
    super();
    this.state = {
      parts: [],
    };
    }

    componentDidMount = () => {
    fetch('http://pisio.etfbl.net/~bojanp/up1/projectpartrest/project/', {
      method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    project_id: this.props.navigation.getParam("project", "").id
  })
  })
      .then((response) => {
        if (response.status == 200) return response.json();
        else {
          alert(this.props.navigation.getParam("project", "").id);
          return [];
        }
      })
      .then((response) => {
        this.setState({
          parts: response,
        });
      });
  };


componentDidUpdate = () => {
    this.componentDidMount();
    return this.render();
  }

    render() {
      return (
        <View >
        <Button title='Create Project Part'
       onPress={() => this.props.navigation.navigate('CreateProjectPart',{project: this.props.navigation.getParam("project", ""),user:this.props.navigation.getParam("user", "")})}> </Button>
        <Text style={styles.tekst}>Project Parts:</Text>
       {this.state.parts.map((part, index) => {
         return (
           <Button title={part.job} onPress={() => this.props.navigation.navigate('ProjectPartDetails', {
             part:part,
             user:this.props.navigation.getParam("user", "")})}> </Button>
         );
      })}
     </View>
      );
    }
  }
  const styles = StyleSheet.create({
    tekst:{
    height:45,
    margin:'0%',
    width:'80%',
    fontSize:20,
  },
  });