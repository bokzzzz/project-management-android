import * as React from 'react';
import { Text,Button, TextInput,View,TouchableOpacity, StyleSheet, Image } from 'react-native';

export default class FinanceDetailsScreen extends React.Component {
    static navigationOptions = {
      title: 'Finance Details',
    };
    render() {
      return (
        <View>
        <View style={styles.container1}>
        <Text style={styles.prviRed} >Description:{this.props.navigation.getParam("finance", "").description}</Text>
        </View>
        <View style={styles.container2}>
        <Text style={styles.drugiRed}>Type:{this.props.navigation.getParam("finance", "").type}</Text>
        </View>
        <View style={styles.container1}>
        <Text style={styles.prviRed}>Amount:{this.props.navigation.getParam("finance", "").amount}</Text>
        </View>
        <View style={styles.container2}>
        <Text style={styles.drugiRed}>Payment Time:{this.props.navigation.getParam("finance", "").payment_time}</Text>
        </View>
        </View>
      );
    }
  }
  const styles = StyleSheet.create({
  prviRed:
  {
    fontSize:22
  },
  drugiRed:
  {
    fontSize:22
  },
  container1: {
    width:'100%',
    backgroundColor: '#e0dede',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    height:45
    
  },
   container2: {
     width:'100%',
    backgroundColor: '#f5f5f5',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    height:45
    
  },
});