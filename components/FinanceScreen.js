import * as React from 'react';
import { Text,Button, TextInput,View,TouchableOpacity, StyleSheet, Image } from 'react-native';

export default class FinanceScreen extends React.Component {
    static navigationOptions = {
      title: 'Finance',
    };

    constructor() {
    super();
    this.state = {
      finances: [],
    };
    }

    componentDidMount = () => {
    fetch('http://pisio.etfbl.net/~bojanp/up1/financerest/project/', {
      method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    project_id: this.props.navigation.getParam("project", "").id
  })
  })
      .then((response) => {
        if (response.status == 200) return response.json();
        else {
          alert(this.props.navigation.getParam("project", "").id);
          return [];
        }
      })
      .then((response) => {
        this.setState({
          finances: response,
        });
      });
  };


 componentDidUpdate = () => {
    this.componentDidMount();
    return this.render();
  }

    render() {
      return (
        <View >
        <Button title='Create Finance'
       onPress={() => this.props.navigation.navigate('CreateFinance',{project: this.props.navigation.getParam("project", ""),user:this.props.navigation.getParam("user", "")})}> </Button>
        <Text style={styles.tekst}>Finances:</Text>
       {this.state.finances.map((finance, index) => {
         return (
           <Button title={finance.description} onPress={() => this.props.navigation.navigate('FinanceDetails', {finance:finance})}> </Button>
         );
      })}
     </View>
      );
    }
  }
  const styles = StyleSheet.create({
    tekst:{
    height:45,
    margin:'0%',
    width:'80%',
    fontSize:20,
  },
  });