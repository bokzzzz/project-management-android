import * as React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, TextInput,ScrollView} from 'react-native';
import DateTimePicker from 'react-native-datepicker';
import PickerSelect from 'react-native-picker-select';
import Constants from 'expo-constants';
import { SafeAreaView,SafeAreaProvider } from 'react-native-safe-area-context';

export default class AddWorkerScreen extends React.Component {
  static navigationOptions = {
      title: 'Add Worker',
    };
  constructor() {
    super();
    this.state = {
      worker: {
        role:"",
        name:"",
        time_spent: "",
        userId:0,
        users:[],
      }
    };
  }

  componentDidMount = () => {
      fetch('http://pisio.etfbl.net/~bojanp/up1/userrest/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (response.status == 200) return response.json();
        else {
          alert('Try later');
          return [];
        }
      })
      .then((response) => {
        let usersItems=[];
        let i=0;
        for (let user of response) {
            usersItems[i++]={label:user.name + " " +user.surname,value:user.id};
            }
            let oldPr=this.state.worker;
            oldPr.users=usersItems;
            this.setState({
              worker:oldPr
            });
      });
  };

  createProject = (event) => {
    console.log(1);
    if(this.state.worker.role !=""  &&
    this.state.worker.userId > 0 && this.state.worker.time_spent > 0){
    fetch('http://pisio.etfbl.net/~bojanp/up1/workerrest/create/', {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    role: this.state.worker.role,
    project_part_id: this.props.navigation.getParam("part", "").id,
    time_spent: this.state.worker.time_spent,
    user_id:this.state.worker.userId,
    auth_key:this.props.navigation.getParam("user", "").auth_key
  })
  }).then(res => {console.log(2);return res.json()})
  .then((data) => {
    console.log(3);
   if(data.message == "Success"){
    alert("Worker successfuly added!");
    this.props.navigation.goBack();
   }
   else alert("Worker not added! " + data.message);
  });
    }else{
      alert("Please, fill all fields!");


    }
    console.log(4);
  }



  render(){
  return (
    <ScrollView>
    <SafeAreaProvider><SafeAreaView>
    <View style={styles.container}>
    
          <Text style={styles.tekst}>Worker:</Text>
          <View  style={styles.datumi}>
          <PickerSelect
            onValueChange={(value) => {
              let odPr=this.state.worker;
            odPr.userId=value;
            this.setState({worker:odPr})
            }}
            style={styles.selekt}
            items={this.state.worker.users}/></View>
      <Text style={styles.tekst}>Role:</Text>
      <TextInput  onChangeText={(role) =>{
            let odPr=this.state.worker;
            odPr.role=role;
            this.setState({worker:odPr})
      }} style={styles.textInput}></TextInput>
      <Text style={styles.tekst}>Time Spent:</Text>
       <TextInput  onChangeText={(time_spent) =>{
            let odPr=this.state.worker;
            odPr.time_spent=time_spent;
            this.setState({worker:odPr})
      }} style={styles.textInput} keyboardType={'numeric'}></TextInput>
           
      <TouchableOpacity
          style={styles.loginScreenButton}
          onPress={
            this.createProject
		  }
		  
          underlayColor='#fff'>
          <Text style={styles.loginText}>Create</Text>
 </TouchableOpacity>
     
    </View>
    </SafeAreaView></SafeAreaProvider>
    </ScrollView>
  );
  }
  
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ecf0f1',
    height:'100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    
  },
  
  textInput:{
    borderColor: '#000000',
    width:'80%',
    height:45,
    borderWidth:2,
    marginBottom:'5%',
    backgroundColor:'white',
    fontSize:20,
    padding:10,
  },
  tekst:{
    height:45,
    margin:'0%',
    width:'80%',
    fontSize:20,
  },
  loginScreenButton:{
    width:'50%',
    height:45,
    
    backgroundColor:'#1E6738',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText:{
      color:'#fff',
      textAlignVertical:'center',
      fontSize:27 ,
      textAlign:'center',
  },
  datumi:{
      width: '80%',
      height:45,
  },
  selekt:{
    color:"black",
    backgroundColor:"black"
  }
});