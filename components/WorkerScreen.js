import * as React from 'react';
import { Text,Button, TextInput,View,TouchableOpacity, StyleSheet, Image } from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';

export default class WorkerScreen extends React.Component {
    static navigationOptions = {
      title: 'Workers',
    };

     constructor() {
    super();
    this.state = {
      tableHead: ['Name', 'Role', 'Time Spent'],
      workers: [],
      tableData:[[]]
    };
    }

    componentDidMount = () => {
    fetch('http://pisio.etfbl.net/~bojanp/up1/workerrest/project/', {
      method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    project_part_id: this.props.navigation.getParam("part", "").id
  })
  })
      .then((response) => {
        if (response.status == 200) return response.json();
        else {
          alert(this.props.navigation.getParam("part", "").id);
          return [];
        }
      })
      .then((response) => {
        this.setState({
          workers: response,
        });
        var data=[[]];
      for(var i=0;i<this.state.workers.length;i++){
        data[i]=[this.state.workers[i].name,this.state.workers[i].role,this.state.workers[i].time_spent];
      }
      this.setState({tableData:data});
      });
      
  };


componentDidUpdate = () => {
    this.componentDidMount();
    return this.render();
  }

    render() {
      return (
        <View style={styles.container}>
        <Button title='Add Worker on Project Part'
       onPress={() => this.props.navigation.navigate('AddWorker',{
         user:this.props.navigation.getParam("user", ""),
         part:this.props.navigation.getParam("part", "")})}> </Button>
       
        <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
          <Row data={this.state.tableHead} style={styles.head} textStyle={styles.text}/>
          <Rows data={this.state.tableData} textStyle={styles.text}/>
        </Table>
      </View>
      );
    }
  }
  const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: { height: 40, backgroundColor: '#f1f8ff' },
  text: { margin: 6 }
});