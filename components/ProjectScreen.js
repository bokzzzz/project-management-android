import * as React from 'react';
import { Text,Button, TextInput,View,TouchableOpacity, StyleSheet, Image,ScrollView } from 'react-native';
import { SafeAreaView,SafeAreaProvider } from 'react-native-safe-area-context';

export default class ProjectScreen extends React.Component {
    static navigationOptions = {
      title: 'Project',
    };
    constructor() {
    super();
    this.state = {
      projects: [],
    };
  }

    componentDidUpdate = () => {
    this.componentDidMount();
    return this.render();
  }

    componentDidMount = () => {
    fetch('http://pisio.etfbl.net/~bojanp/up1/projectrest/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (response.status == 200) return response.json();
        else {
          alert('Try later');
          return [];
        }
      })
      .then((response) => {
        this.setState({
          projects: response,
        });
      });
  };
    render() {
      return (
       <SafeAreaProvider><SafeAreaView>
       <ScrollView>
       <Button title='Create Project'
       onPress={() => this.props.navigation.navigate('CreateProject',{user: this.props.navigation.getParam("user", "")})}> </Button>
       <Text style={styles.tekst}>Projects:</Text>
       {this.state.projects.map((project, index) => {
         return (
           <Button title={project.name} onPress={() => this.props.navigation.navigate('ProjectDetails', {
             project:project,
           user: this.props.navigation.getParam("user", "")
           })}> </Button>
         );
      })}
      </ScrollView>
     </SafeAreaView></SafeAreaProvider>
      );
    }
  }
  const styles = StyleSheet.create({
    tekst:{
    height:45,
    margin:'0%',
    width:'80%',
    fontSize:20,
  },
  });