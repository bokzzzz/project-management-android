import * as React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, TextInput,ScrollView} from 'react-native';
import DateTimePicker from 'react-native-datepicker';
import PickerSelect from 'react-native-picker-select';
import Constants from 'expo-constants';
import { SafeAreaView,SafeAreaProvider } from 'react-native-safe-area-context';

export default class CreateProjectScreen extends React.Component {
  static navigationOptions = {
      title: 'Create Project',
    };
  constructor() {
    super();
    this.state = {
      project: {
        name:"",
        manager:0,
        client:0,
        start_date: "",
        deadline: "",
        end_date: "",
        clients:[],
      }
    };
  }

  componentDidMount = () => {
      fetch('http://pisio.etfbl.net/~bojanp/up1/participantrest/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (response.status == 200) return response.json();
        else {
          alert('Try later');
          return [];
        }
      })
      .then((response) => {
        let clientsItems=[];
        let i=0;
        for (let client of response) {
            clientsItems[i++]={label:client.name,value:client.id};
            }
            let oldPr=this.state.project;
            oldPr.clients=clientsItems;
            this.setState({
              project:oldPr
            });
      });
  };

  createProject = (event) => {
    /*alert(this.state.project.name+
    this.state.project.start_date+
    this.state.project.end_date+
     this.state.project.deadline+
    this.state.project.client+
    this.state.project.manager);
    */
    if(this.state.project.name !="" && this.state.project.start_date !="" &&
    this.state.project.deadline !="" && this.state.project.end_date !=0 &&
    this.state.project.client >0 && this.state.project.manager >0){
    fetch('http://pisio.etfbl.net/~bojanp/up1/projectrest/create/', {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    name: this.state.project.name,
    start_date: this.state.project.start_date,
    end_date: this.state.project.end_date,
    deadline: this.state.project.deadline,
    client: this.state.project.client,
    manager:this.state.project.manager,
    auth_key:this.props.navigation.getParam("user", "").auth_key
  })
  }).then(res => res.json())
  .then((data) => {
   if(data.message == "Success"){
    alert("Dodan");
    this.props.navigation.goBack();
   }
   else alert("Projekat nije dodan");
  });
    }else{
      alert("Popunite sva polja");


    }
  }



  render(){
  return (
    <ScrollView>
    <SafeAreaProvider><SafeAreaView>
    <View style={styles.container}>
      <Text></Text>
      <Text style={styles.tekst}>Name:</Text>
      <TextInput autoCapitalize = 'none' onChangeText={(name) =>{
            let odPr=this.state.project;
            odPr.name=name;
            this.setState({project:odPr})
      }} style={styles.textInput}></TextInput>
      <Text style={styles.tekst}>Start Date:</Text>
      <DateTimePicker
      style={styles.datumi}
          date={this.state.project.start_date} 
          mode="date" 
          placeholder="select date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
          }}
          onDateChange={(date) => {
            let odPr=this.state.project;
            odPr.start_date=date;
            this.setState({project:odPr})
          }}
           ></DateTimePicker>
           <Text style={styles.tekst}>Deadline:</Text>
      <DateTimePicker
      style={styles.datumi}
          date={this.state.project.deadline} 
          mode="date" 
          placeholder="select date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
          }}
          onDateChange={(date) => {
            let odPr=this.state.project;   
            odPr.deadline=date;
            this.setState({project:odPr})
          }}
           ></DateTimePicker>
           <Text style={styles.tekst}>End Date:</Text>
      <DateTimePicker
      style={styles.datumi}
          date={this.state.project.end_date} 
          mode="date" 
          placeholder="select date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
          }}
          onDateChange={(date) => {
            let odPr=this.state.project;
            odPr.end_date=date;
            this.setState({project:odPr})
          }}
           ></DateTimePicker>
           <Text style={styles.tekst}>Manager:</Text>
           <View  style={styles.datumi}>
           <PickerSelect
            onValueChange={(value) => {
              let odPr=this.state.project;
            odPr.manager=value;
            this.setState({project:odPr})
            }}
            style={styles.selekt}
            items={[{'label':this.props.navigation.getParam("user", "").name,'value':this.props.navigation.getParam("user", "").id}]}/></View>
          
           <Text style={styles.tekst}>Client:</Text>
           <View  style={styles.datumi}>
           <PickerSelect
            onValueChange={(value) => {
              let odPr=this.state.project;
            odPr.client=value;
            this.setState({project:odPr})
            }}
            style={styles.selekt}
            items={this.state.project.clients}/></View>
      <TouchableOpacity
          style={styles.loginScreenButton}
          onPress={
            this.createProject
		  }
		  
          underlayColor='#fff'>
          <Text style={styles.loginText}>Create</Text>
 </TouchableOpacity>
     
    </View>
    </SafeAreaView></SafeAreaProvider>
    </ScrollView>
  );
  }
  
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ecf0f1',
    height:'100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    
  },
  
  textInput:{
    borderColor: '#000000',
    width:'80%',
    height:45,
    borderWidth:2,
    marginBottom:'5%',
    backgroundColor:'white',
    fontSize:20,
    padding:10,
  },
  tekst:{
    height:45,
    margin:'0%',
    width:'80%',
    fontSize:20,
  },
  loginScreenButton:{
    width:'50%',
    height:45,
    
    backgroundColor:'#1E6738',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText:{
      color:'#fff',
      textAlignVertical:'center',
      fontSize:27 ,
      textAlign:'center',
  },
  datumi:{
      width: '80%',
      height:45,
  },
  selekt:{
    color:"black",
    backgroundColor:"black"
  }
});