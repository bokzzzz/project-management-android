import * as React from 'react';
import { Text,Button, TextInput,View,TouchableOpacity, StyleSheet, Image } from 'react-native';

export default class App extends React.Component {
  
  constructor() {
    super();
    this.state = {
      user: {
        username: '',
        password: '',
      }
    };
  }
  login = (event) => {
    fetch('http://pisio.etfbl.net/~bojanp/up1/userrest/login/', {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    username: this.state.user.username,
    password: this.state.user.password
  })
  }).then(res => res.json())
  .then((data) => {
   if(data.message === 'True'){
    this.props.navigation.navigate('Project',{user:data.user});
    //alert("Je");
   }
   else alert("Nije");
  });
  }
  changePassword = (value) => {
    let old_cred =  this.state;
    old_cred.user.password = value;
    this.setState({credentials: old_cred});
  };
  changeUsername = (value) => {
    let old_cred =  this.state;
    old_cred.user.username = value;
    this.setState({credentials: old_cred});
  };
  render() {
    return (
      <View style={styles.container}>
      <Text style={styles.tekst}>Username:</Text>
      <TextInput autoCapitalize = 'none' onChangeText={(username) =>
            this.changeUsername(username)
          } style={styles.textInput}></TextInput>
      <Text style={styles.tekst}>Password:</Text>
      <TextInput autoCapitalize = 'none' onChangeText={(pass) =>
            this.changePassword(pass)
          } style={styles.textInput} secureTextEntry={true}></TextInput>
      <TouchableOpacity
          style={styles.loginScreenButton}
          onPress={
            this.login
		  }
		  
          underlayColor='#fff'>
          <Text style={styles.loginText}>Login</Text>
 </TouchableOpacity>
     
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ecf0f1',
    height:'100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    
  },
  
  textInput:{
    borderColor: '#000000',
    width:'80%',
    height:45,
    borderWidth:2,
    marginBottom:'5%',
    backgroundColor:'white',
    fontSize:20,
    padding:10,
  },
  tekst:{
    height:45,
    margin:'0%',
    width:'80%',
    fontSize:20,
  },
  loginScreenButton:{
    width:'50%',
    height:45,
    
    backgroundColor:'#1E6738',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText:{
      color:'#fff',
      textAlignVertical:'center',
      fontSize:27 ,
      textAlign:'center',
  }
});
