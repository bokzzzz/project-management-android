import * as React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, TextInput,ScrollView} from 'react-native';
import DateTimePicker from 'react-native-datepicker';
import PickerSelect from 'react-native-picker-select';
import Constants from 'expo-constants';
import { SafeAreaView,SafeAreaProvider } from 'react-native-safe-area-context';

export default class CreateProjectPartScreen extends React.Component {
  static navigationOptions = {
      title: 'Create Project Part',
    };
  constructor() {
    super();
    this.state = {
      projectPart: {
        job:"",
        manHour:0,
        percentageDone:"",
        start_date: "",
        end_date: "",
      }
    };
  }

  componentDidMount = () => {
      
  };

  createProjectPart = (event) => {
 
    if(this.state.projectPart.job !="" && this.state.projectPart.start_date !="" && this.state.projectPart.end_date !=0 && this.state.projectPart.manHour >=0 && this.state.projectPart.percentageDone >= 0){
    fetch('http://pisio.etfbl.net/~bojanp/up1/projectpartrest/create/', {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    job: this.state.projectPart.job,
    start_date: this.state.projectPart.start_date,
    end_date: this.state.projectPart.end_date,
    man_hour: this.state.projectPart.manHour,
    percentage_done:this.state.projectPart.percentageDone,
    auth_key:this.props.navigation.getParam("user", "").auth_key,
    project_id:this.props.navigation.getParam("project", "").id
  })
  }).then(res => res.json())
  .then((data) => {
   if(data.message == "Success"){
    alert("Project Part successfully added!");
    this.props.navigation.goBack();
   }
   //else alert(data);
  });
    }else{
      alert("Plese, fill all fields!");


    }
  }



  render(){
  return (
    <ScrollView>
    <SafeAreaProvider><SafeAreaView>
    <View style={styles.container}>
      <Text style={styles.tekst}>Job Description:</Text>
      <TextInput  onChangeText={(job) =>{
            let odPr=this.state.projectPart;
            odPr.job=job;
            this.setState({projectPart:odPr})
      }} style={styles.textInput}></TextInput>
       <Text style={styles.tekst}>Man Hour:</Text>
          <TextInput  onChangeText={(manHour) =>{
            let odPr=this.state.projectPart;
            odPr.manHour=manHour;
            this.setState({projectPart:odPr})
      }} style={styles.textInput} keyboardType={'numeric'}></TextInput>
       <Text style={styles.tekst}>Percentage Done:</Text>
          <TextInput  onChangeText={(percentageDone) =>{
            let odPr=this.state.projectPart;
            odPr.percentageDone=percentageDone;
            this.setState({projectPart:odPr})
      }} style={styles.textInput} keyboardType={'numeric'}></TextInput>
      <Text style={styles.tekst}>Start Date:</Text>
      <DateTimePicker
      style={styles.datumi}
          date={this.state.projectPart.start_date} 
          mode="date" 
          placeholder="select date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
          }}
          onDateChange={(date) => {
            let odPr=this.state.projectPart;
            odPr.start_date=date;
            this.setState({projectPart:odPr})
          }}
           ></DateTimePicker>
         
           <Text style={styles.tekst}>End Date:</Text>
      <DateTimePicker
      style={styles.datumi}
          date={this.state.projectPart.end_date} 
          mode="date" 
          placeholder="select date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
          }}
          onDateChange={(date) => {
            let odPr=this.state.projectPart;
            odPr.end_date=date;
            this.setState({projectPart:odPr})
          }}
           ></DateTimePicker>
          

      <TouchableOpacity
          style={styles.loginScreenButton}
          onPress={
            this.createProjectPart
		  }
		  
          underlayColor='#fff'>
          <Text style={styles.loginText}>Create</Text>
 </TouchableOpacity>
     
    </View>
    </SafeAreaView></SafeAreaProvider>
    </ScrollView>
  );
  }
  
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ecf0f1',
    height:'100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    
  },
  
  textInput:{
    borderColor: '#000000',
    width:'80%',
    height:45,
    borderWidth:2,
    marginBottom:'5%',
    backgroundColor:'white',
    fontSize:20,
    padding:10,
  },
  tekst:{
    height:45,
    margin:'0%',
    width:'80%',
    fontSize:20,
  },
  loginScreenButton:{
    width:'50%',
    height:45,
    
    backgroundColor:'#1E6738',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText:{
      color:'#fff',
      textAlignVertical:'center',
      fontSize:27 ,
      textAlign:'center',
  },
  datumi:{
      width: '80%',
      height:45,
  },
});