import * as React from 'react';
import { Text,Button, TextInput,View,TouchableOpacity, StyleSheet, Image } from 'react-native';

export default class ProjectDetailsScreen extends React.Component {
    static navigationOptions = {
      title: 'Project Details',
    };
    render() {
      return (
        <View>
        <View style={styles.container1}>
        <Text style={styles.prviRed} >Name:{this.props.navigation.getParam("project", "").name}</Text>
        </View>
        <View style={styles.container2}>
        <Text style={styles.drugiRed}>Start date:{this.props.navigation.getParam("project", "").start_date}</Text>
        </View>
        <View style={styles.container1}>
        <Text style={styles.prviRed}>End date:{this.props.navigation.getParam("project", "").end_date}</Text>
        </View>
        <View style={styles.container2}>
        <Text style={styles.drugiRed}>Deadline:{this.props.navigation.getParam("project", "").deadline}</Text>
        </View>
        <View style={styles.container1}>
        <Text style={styles.prviRed}>Manager:{this.props.navigation.getParam("project", "").managerName}</Text>
        </View>
        <View style={styles.container2}>
        <Text style={styles.drugiRed}>Client:{this.props.navigation.getParam("project", "").clientName}</Text>
        </View>
        <Button title='View Project Part' onPress={() => this.props.navigation.navigate('ProjectPart', {
          project:this.props.navigation.getParam("project", ""),
          user: this.props.navigation.getParam("user", "")})}>></Button>
        <Button title='View Finance' onPress={() => this.props.navigation.navigate('Finance', {
          project:this.props.navigation.getParam("project", ""),
          user: this.props.navigation.getParam("user", "")
          })}>></Button>
        </View>
      );
    }
  }
  const styles = StyleSheet.create({
  prviRed:
  {
    fontSize:22
  },
  drugiRed:
  {
    fontSize:22
  },
  container1: {
    width:'100%',
    backgroundColor: '#e0dede',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    height:45
    
  },
   container2: {
     width:'100%',
    backgroundColor: '#f5f5f5',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    height:45
    
  },
});