import * as React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, TextInput,ScrollView} from 'react-native';
import DateTimePicker from 'react-native-datepicker';
import PickerSelect from 'react-native-picker-select';
import Constants from 'expo-constants';
import { SafeAreaView,SafeAreaProvider } from 'react-native-safe-area-context';

export default class CreateFinanceScreen extends React.Component {
  static navigationOptions = {
      title: 'Create Finance on Project',
    };
  constructor() {
    super();
    this.state = {
      finance: {
        description:"",
        amount:0,
        type:"",
        payment_time: "",
      }
    };
  }

  componentDidMount = () => {
      
  };

  createProjectPart = (event) => {
 
    if(this.state.finance.description !="" && this.state.finance.type !="" && this.state.finance.payment_time !="" && this.state.finance.amount >=0 ){
    fetch('http://pisio.etfbl.net/~bojanp/up1/financerest/create/', {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    description: this.state.finance.description,
    amount: this.state.finance.amount,
    type: this.state.finance.type,
    payment_time: this.state.finance.payment_time,
    auth_key:this.props.navigation.getParam("user", "").auth_key,
    project_id:this.props.navigation.getParam("project", "").id
  })
  }).then(res => res.json())
  .then((data) => {
   if(data.message == "Success"){
    alert("Finance successfully added!");
    this.props.navigation.goBack();
   }
   else alert(data);
  });
    }else{
      alert("Plese, fill all fields!");


    }
  }



  render(){
  return (
    <ScrollView>
    <SafeAreaProvider><SafeAreaView>
    <View style={styles.container}>
      <Text style={styles.tekst}>Description:</Text>
      <TextInput  onChangeText={(description) =>{
            let odPr=this.state.finance;
            odPr.description=description;
            this.setState({finance:odPr})
      }} style={styles.textInput}></TextInput>
       <Text style={styles.tekst}>Amount:</Text>
          <TextInput  onChangeText={(amount) =>{
            let odPr=this.state.finance;
            odPr.amount=amount;
            this.setState({finance:odPr})
      }} style={styles.textInput} keyboardType={'numeric'}></TextInput>
      <Text style={styles.tekst}>Type:</Text>
      <View  style={styles.datumi}>
           <PickerSelect
            onValueChange={(value) => {
              let odPr=this.state.finance;
            odPr.type=value;
            this.setState({finance:odPr})
            }}
            style={styles.selekt}
            items={[{'label':'Income','value':'Income'},{'label':'Expense','value':'Exponse'}]}/></View>
      <Text style={styles.tekst}>Payment Time:</Text>
      <DateTimePicker
      style={styles.datumi}
          date={this.state.finance.payment_time} 
          mode="date" 
          placeholder="select date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
          }}
          onDateChange={(date) => {
            let odPr=this.state.finance;
            odPr.payment_time=date;
            this.setState({finance:odPr})
          }}
           ></DateTimePicker>
        
          

      <TouchableOpacity
          style={styles.loginScreenButton}
          onPress={
            this.createProjectPart
		  }
		  
          underlayColor='#fff'>
          <Text style={styles.loginText}>Create</Text>
 </TouchableOpacity>
     
    </View>
    </SafeAreaView></SafeAreaProvider>
    </ScrollView>
  );
  }
  
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ecf0f1',
    height:'100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    
  },
  
  textInput:{
    borderColor: '#000000',
    width:'80%',
    height:45,
    borderWidth:2,
    marginBottom:'5%',
    backgroundColor:'white',
    fontSize:20,
    padding:10,
  },
  tekst:{
    height:45,
    margin:'0%',
    width:'80%',
    fontSize:20,
  },
  loginScreenButton:{
    width:'50%',
    height:45,
    
    backgroundColor:'#1E6738',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText:{
      color:'#fff',
      textAlignVertical:'center',
      fontSize:27 ,
      textAlign:'center',
  },
  datumi:{
      width: '80%',
      height:45,
  },
});